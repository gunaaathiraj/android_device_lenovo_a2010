# Release name
PRODUCT_RELEASE_NAME := a2010

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Inherit device configuration
$(call inherit-product, device/lenovo/a2010/device_a2010.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := a2010
PRODUCT_NAME := cm_a2010
PRODUCT_BRAND := lenovo
PRODUCT_MODEL := a2010
PRODUCT_MANUFACTURER := lenovo
